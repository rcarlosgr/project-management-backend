package com.hiper.projectmanagement.services.impl;

import com.hiper.projectmanagement.dtos.task.TaskResponseDTO;
import com.hiper.projectmanagement.entites.Task;
import com.hiper.projectmanagement.exceptions.UnprocessableEntityException;
import com.hiper.projectmanagement.mappers.TaskMapper;
import com.hiper.projectmanagement.repositories.TaskRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class TaskServiceImplTest {
    @Mock
    private TaskRepository taskRepository;

    @Mock
    private TaskMapper taskMapper;

    @InjectMocks
    private TaskServiceImpl taskService;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);

    }

    @Test
    void getAll() throws UnprocessableEntityException {
        // Preparar
        String search = "search";
        String state = "state";
        Pageable pageable = PageRequest.of(0, 10);
        Task task = new Task();
        TaskResponseDTO taskResponseDTO = new TaskResponseDTO();
        List<Task> tasks = Arrays.asList(task);
        Page<Task> taskPage = new PageImpl<>(tasks, pageable, tasks.size());

        when(taskRepository.findAll(search, state, pageable)).thenReturn(taskPage);
        when(taskMapper.toDto(task)).thenReturn(taskResponseDTO);

        // Ejecutar
        Page<TaskResponseDTO> result = taskService.getAll(search, state, pageable);

        // Verificar
        assertEquals(1, result.getContent().size());
        assertEquals(taskResponseDTO, result.getContent().get(0));
        verify(taskRepository, times(1)).findAll(search, state, pageable);
        verify(taskMapper, times(1)).toDto(task);
    }

    @Test
    void getById() throws UnprocessableEntityException {
        // Preparar
        Long id = 1L;
        Task task = new Task();
        TaskResponseDTO taskResponseDTO = new TaskResponseDTO();

        when(taskRepository.findById(id)).thenReturn(Optional.of(task));
        when(taskMapper.toDto(task)).thenReturn(taskResponseDTO);

        // Ejecutar
        TaskResponseDTO result = taskService.getById(id);

        // Verificar
        assertEquals(taskResponseDTO, result);
        verify(taskRepository, times(1)).findById(id);
        verify(taskMapper, times(1)).toDto(task);
    }
}