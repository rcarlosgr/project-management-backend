package com.hiper.projectmanagement.anotations;

import com.hiper.projectmanagement.anotations.impl.UniqueValidatorImpl;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = UniqueValidatorImpl.class)
public @interface UniqueValidator {
    String message() default "El valor ya existe en la base de datos";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
    String fieldName();
    Class<?> entityClass();
}
