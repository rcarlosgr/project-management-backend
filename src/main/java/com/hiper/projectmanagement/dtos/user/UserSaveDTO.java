package com.hiper.projectmanagement.dtos.user;

import com.hiper.projectmanagement.anotations.UniqueValidator;
import com.hiper.projectmanagement.entites.User;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserSaveDTO {
    @NotBlank
    private String name;
    @NotBlank
    @UniqueValidator(entityClass = User.class, fieldName = "email", message = "El email ya está en uso")
    private String email;
}
