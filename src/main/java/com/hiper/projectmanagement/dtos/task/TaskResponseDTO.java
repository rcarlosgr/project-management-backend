package com.hiper.projectmanagement.dtos.task;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.hiper.projectmanagement.dtos.project.ProjectResponseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TaskResponseDTO {
    private Long id;
    private String title;
    private String description;
    private String state;
    private LocalDate startDate;
    private LocalDate endDate;
    protected ProjectResponseDTO project;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdAt;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updatedAt;
}
