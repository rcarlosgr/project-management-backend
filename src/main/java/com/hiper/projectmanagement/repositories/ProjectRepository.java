package com.hiper.projectmanagement.repositories;

import com.hiper.projectmanagement.entites.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    @Query("SELECT p FROM Project p WHERE :search IS NULL OR p.name LIKE %:search% OR p.description LIKE %:search%")
    Page<Project> findAll(String search, Pageable pageable);
}
