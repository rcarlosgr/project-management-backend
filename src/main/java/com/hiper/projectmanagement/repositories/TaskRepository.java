package com.hiper.projectmanagement.repositories;

import com.hiper.projectmanagement.entites.Task;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {
    @Query("SELECT t FROM Task t WHERE (:search IS NULL OR t.title LIKE %:search% OR t.description LIKE %:search%) AND (:state IS NULL OR t.state = :state)")
    Page<Task> findAll(String search, String state, Pageable pageable);
}
