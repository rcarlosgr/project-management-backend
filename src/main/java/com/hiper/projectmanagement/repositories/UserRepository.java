package com.hiper.projectmanagement.repositories;

import com.hiper.projectmanagement.entites.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query("SELECT u FROM User u WHERE :search IS NULL OR u.name LIKE %:search% OR u.email LIKE %:search%")
    Page<User> findAll(String search, Pageable pageable);
    Optional<User> findByEmail(String email);

}
