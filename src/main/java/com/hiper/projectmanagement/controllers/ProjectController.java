package com.hiper.projectmanagement.controllers;

import com.hiper.projectmanagement.dtos.project.ProjectResponseDTO;
import com.hiper.projectmanagement.dtos.project.ProjectSaveDTO;
import com.hiper.projectmanagement.dtos.project.ProjectUpdateDTO;
import com.hiper.projectmanagement.exceptions.UnprocessableEntityException;
import com.hiper.projectmanagement.services.ProjectService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/projects")
@RequiredArgsConstructor
public class ProjectController {
    private final ProjectService projectService;

    @GetMapping("/paginated")
    public ResponseEntity<Page<ProjectResponseDTO>> getAll(
            @RequestParam(required = false) final String search,
            @PageableDefault(page = 0, size = 10, sort = "createdAt", direction = Sort.Direction.DESC) final Pageable pageable
    ) {
        Page<ProjectResponseDTO> projects = projectService.getAll(search, pageable);
        return new ResponseEntity<>(projects, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProjectResponseDTO> getById(@PathVariable Long id) throws UnprocessableEntityException {
        ProjectResponseDTO project = projectService.getById(id);
        return new ResponseEntity<>(project, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ProjectResponseDTO> save(@RequestBody @Valid ProjectSaveDTO projectSaveDTO) throws UnprocessableEntityException {
        ProjectResponseDTO project = projectService.save(projectSaveDTO);
        return new ResponseEntity<>(project, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProjectResponseDTO> update(@PathVariable Long id, @RequestBody @Valid ProjectUpdateDTO projectUpdateDTO) throws UnprocessableEntityException {
        ProjectResponseDTO project = projectService.update(id, projectUpdateDTO);
        return new ResponseEntity<>(project, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) throws UnprocessableEntityException {
        projectService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
