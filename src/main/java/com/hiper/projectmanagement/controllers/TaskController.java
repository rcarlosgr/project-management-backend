package com.hiper.projectmanagement.controllers;

import com.hiper.projectmanagement.dtos.task.TaskResponseDTO;
import com.hiper.projectmanagement.dtos.task.TaskSaveDTO;
import com.hiper.projectmanagement.dtos.task.TaskUpdateDTO;
import com.hiper.projectmanagement.exceptions.UnprocessableEntityException;
import com.hiper.projectmanagement.services.TaskService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/tasks")
@RequiredArgsConstructor
public class TaskController {
    private final TaskService taskService;

    @GetMapping("/paginated")
    public ResponseEntity<Page<TaskResponseDTO>> getAll(
            @RequestParam(required = false) final String search,
            @RequestParam(required = false) final String state,
            @PageableDefault(page = 0, size = 10, sort = "createdAt", direction = Sort.Direction.DESC) final Pageable pageable
    ) {
        Page<TaskResponseDTO> tasks = taskService.getAll(search, state, pageable);
        return new ResponseEntity<>(tasks, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TaskResponseDTO> getById(@PathVariable Long id) throws UnprocessableEntityException {
        TaskResponseDTO task = taskService.getById(id);
        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<TaskResponseDTO> save(@RequestBody @Valid TaskSaveDTO taskSaveDTO) throws UnprocessableEntityException {
        TaskResponseDTO task = taskService.save(taskSaveDTO);
        return new ResponseEntity<>(task, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TaskResponseDTO> update(@PathVariable Long id, @RequestBody @Valid TaskUpdateDTO taskUpdateDTO) throws UnprocessableEntityException {
        TaskResponseDTO task = taskService.update(id, taskUpdateDTO);
        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) throws UnprocessableEntityException {
        taskService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
