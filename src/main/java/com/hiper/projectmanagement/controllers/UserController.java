package com.hiper.projectmanagement.controllers;

import com.hiper.projectmanagement.dtos.user.UserResponseDTO;
import com.hiper.projectmanagement.dtos.user.UserSaveDTO;
import com.hiper.projectmanagement.dtos.user.UserUpdateDTO;
import com.hiper.projectmanagement.exceptions.UnprocessableEntityException;
import com.hiper.projectmanagement.services.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/paginated")
    public ResponseEntity<Page<UserResponseDTO>> getAll(
            @RequestParam(required = false) final String search,
            @PageableDefault(page = 0, size = 10, sort = "createdAt", direction = Sort.Direction.DESC) final Pageable pageable
    ) {
        Page<UserResponseDTO> users = userService.getAll(search, pageable);
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserResponseDTO> getById(@PathVariable Long id) throws UnprocessableEntityException {
        UserResponseDTO user = userService.getById(id);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("/email/{email}")
    public ResponseEntity<UserResponseDTO> getByEmail(@PathVariable String email) throws UnprocessableEntityException {
        UserResponseDTO user = userService.getByEmail(email);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UserResponseDTO> save(@RequestBody @Valid UserSaveDTO userSaveDTO) throws UnprocessableEntityException {
        UserResponseDTO user = userService.save(userSaveDTO);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserResponseDTO> update(@PathVariable Long id, @RequestBody @Valid UserUpdateDTO userUpdateDTO) throws UnprocessableEntityException {
        UserResponseDTO user = userService.update(id, userUpdateDTO);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) throws UnprocessableEntityException {
        userService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
