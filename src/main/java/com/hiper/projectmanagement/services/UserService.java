package com.hiper.projectmanagement.services;

import com.hiper.projectmanagement.dtos.user.UserResponseDTO;
import com.hiper.projectmanagement.dtos.user.UserSaveDTO;
import com.hiper.projectmanagement.dtos.user.UserUpdateDTO;
import com.hiper.projectmanagement.exceptions.UnprocessableEntityException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {
    Page<UserResponseDTO> getAll(String search, Pageable pageable);
    UserResponseDTO getById(Long id) throws UnprocessableEntityException;
    UserResponseDTO getByEmail(String email) throws UnprocessableEntityException;
    UserResponseDTO save(UserSaveDTO userSaveDTO) throws UnprocessableEntityException;
    UserResponseDTO update(Long id, UserUpdateDTO userUpdateDTO) throws UnprocessableEntityException;
    void delete(Long id) throws UnprocessableEntityException;
}
