package com.hiper.projectmanagement.services.impl;

import com.hiper.projectmanagement.dtos.task.TaskResponseDTO;
import com.hiper.projectmanagement.dtos.task.TaskSaveDTO;
import com.hiper.projectmanagement.dtos.task.TaskUpdateDTO;
import com.hiper.projectmanagement.entites.Project;
import com.hiper.projectmanagement.entites.Task;
import com.hiper.projectmanagement.exceptions.UnprocessableEntityException;
import com.hiper.projectmanagement.mappers.ProjectMapper;
import com.hiper.projectmanagement.mappers.TaskMapper;
import com.hiper.projectmanagement.repositories.ProjectRepository;
import com.hiper.projectmanagement.repositories.TaskRepository;
import com.hiper.projectmanagement.services.TaskService;
import com.hiper.projectmanagement.utils.StringUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;
    private final ProjectRepository projectRepository;
    private final TaskMapper taskMapper;
    private final ProjectMapper projectMapper;

    @Override
    public Page<TaskResponseDTO> getAll(String search, String state, Pageable pageable) {
        return taskRepository.findAll(search, state, pageable)
                .map(taskMapper::toDto);
    }

    @Override
    public TaskResponseDTO getById(Long id) throws UnprocessableEntityException {
        Task task = taskRepository.findById(id).orElseThrow(() -> new UnprocessableEntityException("id", "No existe registro"));
        return taskMapper.toDto(task);
    }

    @Override
    public TaskResponseDTO save(TaskSaveDTO taskSaveDTO) throws UnprocessableEntityException {
        Project project = projectRepository.findById(taskSaveDTO.getProjectId()).orElseThrow(() -> new UnprocessableEntityException("id", "No existe proyecto"));

        taskSaveDTO.setTitle(StringUtil.trimAndNullifyEmptyString(taskSaveDTO.getTitle()));
        taskSaveDTO.setDescription(StringUtil.trimAndNullifyEmptyString(taskSaveDTO.getDescription()));
        taskSaveDTO.setState(StringUtil.trimAndNullifyEmptyString(taskSaveDTO.getState()));

        Task task = taskMapper.toEntity(taskSaveDTO);
        task.setProject(project);
        return taskMapper.toDto(taskRepository.save(task));
    }

    @Override
    public TaskResponseDTO update(Long id, TaskUpdateDTO taskUpdateDTO) throws UnprocessableEntityException {
        Task task = taskRepository.findById(id).orElseThrow(() -> new UnprocessableEntityException("id", "No existe registro"));
        Project project =  projectRepository.findById(taskUpdateDTO.getProjectId()).orElseThrow(() -> new UnprocessableEntityException("id", "No existe proyecto"));

        taskUpdateDTO.setTitle(StringUtil.trimAndNullifyEmptyString(taskUpdateDTO.getTitle()));
        taskUpdateDTO.setDescription(StringUtil.trimAndNullifyEmptyString(taskUpdateDTO.getDescription()));
        taskUpdateDTO.setState(StringUtil.trimAndNullifyEmptyString(taskUpdateDTO.getState()));

        taskMapper.updateEntityFromDto(taskUpdateDTO, task);
        task.setProject(project);
        return taskMapper.toDto(taskRepository.save(task));
    }

    @Override
    public void delete(Long id) throws UnprocessableEntityException {
        taskRepository.findById(id).orElseThrow(() -> new UnprocessableEntityException("id", "No existe registro"));
        taskRepository.deleteById(id);
    }
}
