package com.hiper.projectmanagement.services.impl;

import com.hiper.projectmanagement.dtos.user.UserResponseDTO;
import com.hiper.projectmanagement.dtos.user.UserSaveDTO;
import com.hiper.projectmanagement.dtos.user.UserUpdateDTO;
import com.hiper.projectmanagement.entites.User;
import com.hiper.projectmanagement.exceptions.UnprocessableEntityException;
import com.hiper.projectmanagement.mappers.UserMapper;
import com.hiper.projectmanagement.repositories.UserRepository;
import com.hiper.projectmanagement.services.UserService;
import com.hiper.projectmanagement.utils.StringUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Override
    public Page<UserResponseDTO> getAll(String search, Pageable pageable) {
        return userRepository.findAll(search, pageable)
                .map(userMapper::toDto);
    }

    @Override
    public UserResponseDTO getById(Long id) throws UnprocessableEntityException {
        User user = userRepository.findById(id).orElseThrow(() -> new UnprocessableEntityException("id", "No existe registro"));
        return userMapper.toDto(user);
    }

    @Override
    public UserResponseDTO getByEmail(String email) throws UnprocessableEntityException {
        User user = userRepository.findByEmail(email).orElseThrow(() -> new UnprocessableEntityException("email", "No existe registro"));
        return userMapper.toDto(user);
    }

    @Override
    public UserResponseDTO save(UserSaveDTO userSaveDTO) throws UnprocessableEntityException {
        userSaveDTO.setName(StringUtil.trimAndNullifyEmptyString(userSaveDTO.getName()));
        userSaveDTO.setEmail(StringUtil.trimAndNullifyEmptyString(userSaveDTO.getEmail()));

        User user = userMapper.toEntity(userSaveDTO);
        return userMapper.toDto(userRepository.save(user));
    }

    @Override
    public UserResponseDTO update(Long id, UserUpdateDTO userUpdateDTO) throws UnprocessableEntityException {
        User user = userRepository.findById(id).orElseThrow(() -> new UnprocessableEntityException("id", "No existe registro"));
        User emailUser = userRepository.findByEmail(userUpdateDTO.getEmail()).orElse(null);
        if (emailUser != null && !userUpdateDTO.getEmail().equals(user.getEmail())) throw new UnprocessableEntityException("email", "Correo eletrónico ya está en uso");

        userUpdateDTO.setName(StringUtil.trimAndNullifyEmptyString(userUpdateDTO.getName()));
        userUpdateDTO.setEmail(StringUtil.trimAndNullifyEmptyString(userUpdateDTO.getEmail()));

        userMapper.updateEntityFromDto(userUpdateDTO, user);
        return userMapper.toDto(userRepository.save(user));
    }

    @Override
    public void delete(Long id) throws UnprocessableEntityException {
        userRepository.findById(id).orElseThrow(() -> new UnprocessableEntityException("id", "No existe registro"));
        userRepository.deleteById(id);
    }
}
