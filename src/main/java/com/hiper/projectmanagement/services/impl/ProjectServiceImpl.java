package com.hiper.projectmanagement.services.impl;

import com.hiper.projectmanagement.dtos.project.ProjectResponseDTO;
import com.hiper.projectmanagement.dtos.project.ProjectSaveDTO;
import com.hiper.projectmanagement.dtos.project.ProjectUpdateDTO;
import com.hiper.projectmanagement.entites.Project;
import com.hiper.projectmanagement.exceptions.UnprocessableEntityException;
import com.hiper.projectmanagement.mappers.ProjectMapper;
import com.hiper.projectmanagement.repositories.ProjectRepository;
import com.hiper.projectmanagement.services.ProjectService;
import com.hiper.projectmanagement.utils.StringUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {
    private final ProjectRepository projectRepository;
    private final ProjectMapper projectMapper;

    @Override
    public Page<ProjectResponseDTO> getAll(String search, Pageable pageable) {
        return projectRepository.findAll(search, pageable)
                .map(projectMapper::toDto);
    }

    @Override
    public ProjectResponseDTO getById(Long id) throws UnprocessableEntityException {
        Project project = projectRepository.findById(id).orElseThrow(() -> new UnprocessableEntityException("id", "No existe registro"));
        return projectMapper.toDto(project);
    }

    @Override
    public ProjectResponseDTO save(ProjectSaveDTO projectSaveDTO) throws UnprocessableEntityException {
        projectSaveDTO.setName(StringUtil.trimAndNullifyEmptyString(projectSaveDTO.getName()));
        projectSaveDTO.setDescription(StringUtil.trimAndNullifyEmptyString(projectSaveDTO.getDescription()));

        Project project = projectMapper.toEntity(projectSaveDTO);
        return projectMapper.toDto(projectRepository.save(project));
    }

    @Override
    public ProjectResponseDTO update(Long id, ProjectUpdateDTO projectUpdateDTO) throws UnprocessableEntityException {
        Project project = projectRepository.findById(id).orElseThrow(() -> new UnprocessableEntityException("id", "No existe registro"));

        projectUpdateDTO.setName(StringUtil.trimAndNullifyEmptyString(projectUpdateDTO.getName()));
        projectUpdateDTO.setDescription(StringUtil.trimAndNullifyEmptyString(projectUpdateDTO.getDescription()));

        projectMapper.updateEntityFromDto(projectUpdateDTO, project);
        return projectMapper.toDto(projectRepository.save(project));
    }

    @Override
    public void delete(Long id) throws UnprocessableEntityException {
        projectRepository.findById(id).orElseThrow(() -> new UnprocessableEntityException("id", "No existe registro"));
        projectRepository.deleteById(id);

    }
}
