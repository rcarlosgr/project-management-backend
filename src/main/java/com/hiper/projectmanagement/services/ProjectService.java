package com.hiper.projectmanagement.services;

import com.hiper.projectmanagement.dtos.project.ProjectResponseDTO;
import com.hiper.projectmanagement.dtos.project.ProjectSaveDTO;
import com.hiper.projectmanagement.dtos.project.ProjectUpdateDTO;
import com.hiper.projectmanagement.exceptions.UnprocessableEntityException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProjectService {
    Page<ProjectResponseDTO> getAll(String search, Pageable pageable);
    ProjectResponseDTO getById(Long id) throws UnprocessableEntityException;
    ProjectResponseDTO save(ProjectSaveDTO projectSaveDTO) throws UnprocessableEntityException;
    ProjectResponseDTO update(Long id, ProjectUpdateDTO projectUpdateDTO) throws UnprocessableEntityException;
    void delete(Long id) throws UnprocessableEntityException;
}
