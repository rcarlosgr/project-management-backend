package com.hiper.projectmanagement.services;

import com.hiper.projectmanagement.dtos.task.TaskResponseDTO;
import com.hiper.projectmanagement.dtos.task.TaskSaveDTO;
import com.hiper.projectmanagement.dtos.task.TaskUpdateDTO;
import com.hiper.projectmanagement.exceptions.UnprocessableEntityException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface TaskService {
    Page<TaskResponseDTO> getAll(String search, String state, Pageable pageable);
    TaskResponseDTO getById(Long id) throws UnprocessableEntityException;
    TaskResponseDTO save(TaskSaveDTO taskSaveDTO) throws UnprocessableEntityException;
    TaskResponseDTO update(Long id, TaskUpdateDTO taskUpdateDTO) throws UnprocessableEntityException;
    void delete(Long id) throws UnprocessableEntityException;
}
