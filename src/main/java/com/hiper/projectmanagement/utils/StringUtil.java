package com.hiper.projectmanagement.utils;

public class StringUtil {
    /**
     * valida si un texto es vacío
     * @param str texto
     * @return true o false
     */
    public static boolean isNullOrEmpty(String str) {
        return str == null || str.isBlank();
    }

    /**
     * recorta espacios al princio y al final
     * @param str text
     * @return texto recortado
     */
    public static String trimAndNullifyEmptyString(String str) {
        return (str == null || str.isBlank()) ? null : str.strip();
    }
}
