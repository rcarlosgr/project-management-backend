package com.hiper.projectmanagement.mappers;

import com.hiper.projectmanagement.dtos.project.ProjectResponseDTO;
import com.hiper.projectmanagement.dtos.project.ProjectSaveDTO;
import com.hiper.projectmanagement.dtos.project.ProjectUpdateDTO;
import com.hiper.projectmanagement.entites.Project;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface ProjectMapper {
    Project toEntity(ProjectSaveDTO dto);
    void updateEntityFromDto(ProjectUpdateDTO dto, @MappingTarget Project entity);
    ProjectResponseDTO toDto(Project entity);
}
