package com.hiper.projectmanagement.mappers;

import com.hiper.projectmanagement.dtos.user.UserResponseDTO;
import com.hiper.projectmanagement.dtos.user.UserSaveDTO;
import com.hiper.projectmanagement.dtos.user.UserUpdateDTO;
import com.hiper.projectmanagement.entites.User;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User toEntity(UserSaveDTO dto);
    void updateEntityFromDto(UserUpdateDTO dto, @MappingTarget User entity);
    UserResponseDTO toDto(User entity);
}
