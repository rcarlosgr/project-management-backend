package com.hiper.projectmanagement.mappers;

import com.hiper.projectmanagement.dtos.task.TaskResponseDTO;
import com.hiper.projectmanagement.dtos.task.TaskSaveDTO;
import com.hiper.projectmanagement.dtos.task.TaskUpdateDTO;
import com.hiper.projectmanagement.entites.Task;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface TaskMapper {
    Task toEntity(TaskSaveDTO dto);
    void updateEntityFromDto(TaskUpdateDTO dto, @MappingTarget Task entity);
    TaskResponseDTO toDto(Task entity);
}
